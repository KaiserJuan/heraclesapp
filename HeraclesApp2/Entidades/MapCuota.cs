using System;
using System.Text;
using System.Collections.Generic;


namespace HeraclesApp2.Entidades {
    
    public class MapCuota {
        public virtual int IdCuota { get; set; }
        public virtual MapSocios Socios { get; set; }
        public virtual int NumeroDeCuota { get; set; }
        public virtual DateTime FechaPago { get; set; }
        public virtual DateTime FechaVenc { get; set; }
        public virtual float Pago { get; set; }
    }
}
