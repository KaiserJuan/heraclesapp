using System;
using System.Text;
using System.Collections.Generic;


namespace HeraclesApp2.Entidades {
    
    public class MapAdministradores {
        public virtual int IdAdministrador { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Telefono { get; set; }
    }
}
