using System;
using System.Text;
using System.Collections.Generic;


namespace HeraclesApp2.Entidades {
    
    public class MapPase {
        public virtual int IdPase { get; set; }
        public virtual string Tipo { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual float Precio { get; set; }
    }
}
