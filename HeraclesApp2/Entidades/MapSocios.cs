using System;
using System.Text;
using System.Collections.Generic;


namespace HeraclesApp2.Entidades {
    
    public class MapSocios {
        public virtual int IdSocios { get; set; }
        public virtual MapRutinas Rutinas { get; set; }
        public virtual MapPase Pase { get; set; }
        public virtual string NombreCompleto { get; set; }
        public virtual string Telefono { get; set; }
        public virtual float? Peso { get; set; }
        public virtual float? Altura { get; set; }
        public virtual string Direccion { get; set; }
        public virtual string Departamento { get; set; }
        public virtual DateTime FechaIngreso { get; set; }
        public virtual float AFavor { get; set; }
        public virtual float Deuda { get; set; }
        public virtual IList<MapCuota> CuotasPagas { get; set; }

    }
}
