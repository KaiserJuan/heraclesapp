using System;
using System.Text;
using System.Collections.Generic;


namespace HeraclesApp2.Entidades {
    
    public class MapRutinasHasEntrenadores {
        public virtual int RutinasIdRutinas { get; set; }
        public virtual int EntrenadoresIdEntrendores { get; set; }
        public virtual DateTime FechaIni { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as MapRutinasHasEntrenadores;
			if (t == null) return false;
			if (RutinasIdRutinas == t.RutinasIdRutinas
			 && EntrenadoresIdEntrendores == t.EntrenadoresIdEntrendores
			 && FechaIni == t.FechaIni)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ RutinasIdRutinas.GetHashCode();
			hash = (hash * 397) ^ EntrenadoresIdEntrendores.GetHashCode();
			hash = (hash * 397) ^ FechaIni.GetHashCode();

			return hash;
        }
        #endregion
    }
}
