using System;
using System.Text;
using System.Collections.Generic;


namespace HeraclesApp2.Entidades {
    
    public class MapEntrenadores {
        public virtual int IdEntrendores { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Telefono { get; set; }
        public virtual string Direccion { get; set; }
        public virtual string Departamento { get; set; }
        public virtual string Sueldo { get; set; }
    }
}
