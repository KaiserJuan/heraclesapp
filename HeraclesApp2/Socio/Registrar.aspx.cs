﻿using HeraclesApp2.Entidades;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeraclesApp2.Socio
{
    public partial class Registrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            ITransaction transaccion = sesion.BeginTransaction();
            ICriteria sc = sesion.CreateCriteria(typeof(MapPase));
            transaccion.Commit();
            int i = 0;
            try
            {
                foreach (MapPase p in sc.List())
                {
                    ListItem l = new ListItem(p.Tipo, p.IdPase.ToString(), true);
                    input7PaseID.Items.Add(l);
                    i++;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);

            }
            MapSocios socio = null;
            bool cargado = false;
            int id;
            if (this.Session["id_socio"] != null)
            {
                id = int.Parse(this.Session["id_socio"].ToString());
                socio = sesion.Get<MapSocios>(id);
            }

            if (this.Session["cargado"] != null) {
                cargado = bool.Parse(this.Session["cargado"].ToString());
            }

            
            if (socio == null)
                return;
            else if(!cargado){
                input1NyA.Value = socio.NombreCompleto;
                input2Tel.Value = socio.Telefono;
                input3Dir.Value = socio.Direccion;
                input6Dpto.Value = socio.Departamento;
                input4Peso.Value = socio.Peso.ToString();
                input5Altura.Value = socio.Altura.ToString();
                input7PaseID.SelectedValue = socio.Pase.IdPase.ToString();
                this.Session["cargado"] = true;
            }
        }

        public void Registar(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            ITransaction transaction = sesion.BeginTransaction();

            MapSocios aux = null;
            int id;

            if (this.Session["id_socio"] != null)
            {
                id = int.Parse(this.Session["id_socio"].ToString());
                aux = sesion.Get<MapSocios>(id);
            }

            if(aux == null) { 
                MapSocios socio = new MapSocios();
                socio.NombreCompleto = input1NyA.Value;
                socio.Telefono = input2Tel.Value;
                socio.Direccion = input3Dir.Value;
                socio.Departamento = input6Dpto.Value;
                socio.Peso = float.Parse(input4Peso.Value);
                socio.Altura = float.Parse(input5Altura.Value);
                socio.Deuda = 0;
                socio.AFavor = 0;
                socio.FechaIngreso = DateTime.Now;
                socio.Pase = sesion.Get<MapPase>(int.Parse(input7PaseID.SelectedValue)); 
                try
                {
                    sesion.Save(socio);
                    sesion.Flush();
                    transaction.Commit();
                    Response.Redirect("Listar.aspx");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    transaction.Rollback();
                }

                sesion.Close();
                sesiones.Close();
            }
            else
            {
                aux.NombreCompleto = input1NyA.Value;
                aux.Telefono = input2Tel.Value;
                aux.Direccion = input3Dir.Value;
                aux.Departamento = input6Dpto.Value;
                aux.Peso = float.Parse(input4Peso.Value);
                aux.Altura = float.Parse(input5Altura.Value);
                aux.Pase = sesion.Get<MapPase>(int.Parse(input7PaseID.SelectedValue));
                try
                {
                    sesion.Update(aux);
                    sesion.Flush();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    transaction.Rollback();
                }
                this.Session["id_socio"] = null;
                Response.Redirect("Listar.aspx");
                sesion.Close();
                sesiones.Close();
            }
        }
    }
}