﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeraclesApp2.Socio
{
    public partial class Estadistica : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string myScript = "\n<script type=\"text/javascript\" language=\"Javascript\" id=\"EventScriptBlock\">\n";
            string aux = "var auxX = []; for(var i = 1; i < 13; i++) auxX.push(i);" +
                "var auxY = []; for(var i = 1; i < 13; i++) auxY.push(Math.round(Math.random()*100));" +
                "var data = [{ x: auxX," + "y: auxY, type: 'bar', name: 'Cantidad de Inscripciones por Mes'  }];  Plotly.newPlot('myDiv', data); ;";
            myScript += aux;
            myScript += "\n\n </script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, false);
        }
    }
}