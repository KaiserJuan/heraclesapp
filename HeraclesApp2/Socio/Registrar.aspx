﻿<%@ Page Title="Registrar Socio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrar.aspx.cs" Inherits="HeraclesApp2.Socio.Registrar" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h2><%: Title %>.</h2>
    <br />

    <div class="card text-white bg-secondary mb-3">
        <div class="card-header">Registre un Socio</div>
        <div class="card-body">
            <h5 class="card-title">Datos del Socio</h5>
            <p class="card-text">Algunos datos del socio son obligatorios como su nombre y telefono, pero otros como su direccion, departamento y demas son usados para
                sacar estadisticas y graficos para el propio gimnasio y por tanto no son de vital importancia aunque se recomienda cargarlos.</p>
            <div>
                <div class="form-group row">
                    <label for="input1NyA" class="col-sm-2 col-form-label">* Nombre y Apellido</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" runat="server" id="input1NyA" placeholder="Nombre y Apellido">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input2Tel" class="col-sm-2 col-form-label">* Telefono</label>
                    <div class="col-sm-10">
                        <input type="tel" class="form-control" runat="server" id="input2Tel" placeholder="Telefono">
                    </div>
                </div>

                <hr>

                <div class="form-group form-row">
                    <div class="col-4">
                      <label for="input3Dir">Direccion</label>
                      <input type="text" class="form-control" runat="server" id="input3Dir" placeholder="Direccion">
                    </div>
                    <div class="form-group col-3">
                      <label for="input6Dpto">Departamento</label>
                      <select runat="server" id="input6Dpto" class="form-control">
                        <option selected>Capital</option>
                        <option>Santa Lucía</option>
                        <option>Rawson</option>
                        <option>Calingasta</option>
                        <option>Iglesia</option>
                        <option>Jáchal</option>
                        <option>Caucete</option>
                        <option>Valle Fértil</option>
                        <option>25 de Mayo</option>
                        <option>Ullum</option>
                        <option>Sarmiento</option>
                        <option>Zonda</option>
                        <option>Angaco</option>
                        <option>Albardón</option>
                        <option>Pocito</option>
                        <option>San Martín</option>
                        <option>9 de Julio</option>
                        <option>Rivadavia</option>
                        <option>Chimbas</option>
                      </select>
                    </div>
                    <div class="col">
                      <label for="input4Peso">Peso</label>
                      <input type="number" class="form-control" runat="server" id="input4Peso" placeholder="Peso">
                    </div>
                    <div class="col">
                      <label for="input5Altura">Altura</label>
                      <input type="number" class="form-control" runat="server" id="input5Altura" placeholder="Altura">
                    </div>
              </div>

              <hr>
              
              <div class="form-group form-row">
                    <div class="form-group col-7">
                      <label for="input7Pase">Pase</label>
                      <asp:DropDownList runat="server" ID="input7PaseID" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="col">
                      <label for="input8Pago">* Pago</label>
                      <input type="number" class="form-control" runat="server" id="input8Pago" placeholder="Pago">
                    </div>
              </div>
              <div class="text-right">
                  <asp:Button runat="server" ID="action1Registrar" Text="Registrar" OnClick="Registar" CssClass="btn btn-primary btn-lg"/>
              </div>
            </div>
        </div>
    </div>
</asp:Content>