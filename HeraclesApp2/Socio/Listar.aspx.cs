﻿using HeraclesApp2.Entidades;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeraclesApp2.Socio
{
    public partial class Listar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            ITransaction transaccion = sesion.BeginTransaction();
            ICriteria sc = sesion.CreateCriteria(typeof(MapSocios));
            transaccion.Commit();

            Tabla.Rows.Clear();
            TableHeaderRow tr = new TableHeaderRow();
            //tr.Attributes.Add("class", "text-primary text-center h4");
            TableHeaderCell cell1 = new TableHeaderCell();
            cell1.Attributes.Add("scope", "col");
            TableHeaderCell cell2 = new TableHeaderCell();
            cell2.Attributes.Add("scope", "col");
            TableHeaderCell cell3 = new TableHeaderCell();
            cell3.Attributes.Add("scope", "col");
            TableHeaderCell cell4 = new TableHeaderCell();
            cell4.Attributes.Add("scope", "col");
            TableHeaderCell cell5 = new TableHeaderCell();
            cell5.Attributes.Add("scope", "col");
            TableHeaderCell cell6 = new TableHeaderCell();
            cell6.Attributes.Add("scope", "col");
            TableHeaderCell cell7 = new TableHeaderCell();
            cell7.Attributes.Add("scope", "col");
            TableHeaderCell cell8 = new TableHeaderCell();
            cell8.Attributes.Add("scope", "col");
            TableHeaderCell cell9 = new TableHeaderCell();
            cell9.Attributes.Add("scope", "col");
            TableHeaderCell cell10 = new TableHeaderCell();
            cell10.Attributes.Add("scope", "col");
            TableHeaderCell cell11 = new TableHeaderCell();
            cell11.Attributes.Add("scope", "col");
            TableHeaderCell cell12 = new TableHeaderCell();
            cell12.Attributes.Add("scope", "col");

            cell1.Text = "#ID";
            cell2.Text = "Nombre Completo";
            cell3.Text = "Telefono";
            cell4.Text = "Peso";
            cell5.Text = "Altura";
            cell6.Text = "Direccion";
            cell7.Text = "Fecha de Ingreso";
            cell8.Text = "Saldo a Favor";
            cell9.Text = "Deuda";
            cell10.Text = "Rutina";
            cell11.Text = "Pase";
            
            cell12.Text = "Acciones";
            tr.Cells.Add(cell1);
            tr.Cells.Add(cell2);
            tr.Cells.Add(cell3);
            tr.Cells.Add(cell4);
            tr.Cells.Add(cell5);
            tr.Cells.Add(cell6);
            tr.Cells.Add(cell7);
            tr.Cells.Add(cell8);
            tr.Cells.Add(cell9);
            tr.Cells.Add(cell10);
            tr.Cells.Add(cell11);
            tr.Cells.Add(cell12);
            Tabla.Rows.Add(tr);
            int i = 1;
            try
            {
                foreach (MapSocios p in sc.List())
                {
                    TableRow Atr = new TableRow();
                    TableCell Acell1 = new TableCell();
                    TableCell Acell2 = new TableCell();
                    TableCell Acell3 = new TableCell();
                    TableCell Acell4 = new TableCell();
                    TableCell Acell5 = new TableCell();
                    TableCell Acell6 = new TableCell();
                    TableCell Acell7 = new TableCell();
                    TableCell Acell8 = new TableCell();
                    TableCell Acell9 = new TableCell();
                    TableCell Acell10 = new TableCell();
                    TableCell Acell11 = new TableCell();

                    Acell1.Text = i.ToString();
                    Acell1.Attributes.Add("class", "h6");
                    Acell2.Text = p.NombreCompleto;
                    Acell3.Text = p.Telefono;
                    Acell4.Text = p.Peso.ToString() + "kg";
                    Acell5.Text = p.Altura.ToString() + "cm";
                    Acell6.Text = p.Direccion;
                    Acell7.Text = p.FechaIngreso.ToString();
                    Acell8.Text = "$" + p.AFavor.ToString();
                    Acell9.Text = "$" + p.Deuda.ToString();
                    Acell10.Text = p.Pase.Tipo;
                    if (p.Rutinas != null)
                        Acell11.Text = p.Rutinas.Descripcion.ToString();
                    else Acell11.Text = "SD";

                    /* Acciones */
                    TableCell Acell12 = new TableCell();
                    Acell12.Attributes.Add("class", "btn-group-vertical");

                    Button btn = new Button();
                    btn.Text = "Editar";
                    btn.ID = p.IdSocios.ToString() + "_editar";
                    btn.Attributes.Add("class", "btn btn-outline-success");
                    btn.Click += new EventHandler(Editar);
                    Acell12.Controls.Add(btn);

                    Button btn2 = new Button();
                    btn2.Text = "Eliminar";
                    btn2.ID = p.IdSocios.ToString() + "_eliminar";
                    btn2.Attributes.Add("class", "btn btn-outline-danger");
                    btn2.Click += new EventHandler(Eliminar);
                    Acell12.Controls.Add(btn2);

                    Atr.Cells.Add(Acell1);
                    Atr.Cells.Add(Acell2);
                    Atr.Cells.Add(Acell3);
                    Atr.Cells.Add(Acell4);
                    Atr.Cells.Add(Acell5);
                    Atr.Cells.Add(Acell6);
                    Atr.Cells.Add(Acell7);
                    Atr.Cells.Add(Acell8);
                    Atr.Cells.Add(Acell9);
                    Atr.Cells.Add(Acell10);
                    Atr.Cells.Add(Acell11);
                    Atr.Cells.Add(Acell12);
                    Tabla.Rows.Add(Atr);

                    i++;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);

            }
        }

        private void Eliminar(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            ITransaction transaction = sesion.BeginTransaction();
            MapSocios entidad = sesion.Get<MapSocios>(int.Parse((sender as Button).ID.Split('_')[0]));
            try
            {
                sesion.Delete(entidad);
                sesion.Flush();
                transaction.Commit();
                this.Page_Load(sender, e);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                transaction.Rollback();
            }
            sesion.Close();
        }

        private void Editar(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            MapSocios entidad = sesion.Get<MapSocios>(int.Parse((sender as Button).ID.Split('_')[0]));
            this.Session["id_socio"] = entidad.IdSocios;
            Response.Redirect("Registrar.aspx");
            sesion.Close();
        }
    }
}