﻿<%@ Page Title="Estadisticas" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Estadistica.aspx.cs" Inherits="HeraclesApp2.Socio.Estadistica" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="card border-secondary mb-3" >
      <div class="card-header">Cantidad de Inscriptos por Mes</div>
      <div class="card-body text-secondary">
              <div id="myDiv"><!-- Plotly chart will be drawn inside this DIV --></div>

      </div>
    </div>
    <div class="card border-secondary mb-3" >
      <div class="card-header">Comparativas Bianuales</div>
      <div class="card-body text-secondary">
    <div id="myDiv2"><!-- Plotly chart will be drawn inside this DIV --></div>

      </div>
    </div>
    <script>
            Plotly.d3.csv("https://raw.githubusercontent.com/plotly/datasets/master/finance-charts-apple.csv", function(err, rows){

              function unpack(rows, key) {
              return rows.map(function(row) { return row[key]; });
            }

  
            var trace1 = {
              type: "scatter",
              mode: "lines",
              name: '2015-2017',
              x: unpack(rows, 'Date'),
              y: unpack(rows, 'AAPL.High'),
              line: {color: '#17BECF'}
            }

            var trace2 = {
              type: "scatter",
              mode: "lines",
              name: '2012-2014',
              x: unpack(rows, 'Date'),
              y: unpack(rows, 'AAPL.Low'),
              line: {color: '#7F7F7F'}
            }

            var data = [trace1,trace2];
    
            var layout = {
              title: 'Ganancias Bianuales constrastadas', 
            };

            Plotly.newPlot('myDiv2', data, layout);
            })
    </script>
</asp:Content>