﻿using HeraclesApp2.Entidades;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeraclesApp2.Pase
{
    public partial class Listar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            ITransaction transaccion = sesion.BeginTransaction();
            ICriteria sc = sesion.CreateCriteria(typeof(MapPase));
            transaccion.Commit();

            Tabla.Rows.Clear();
            TableHeaderRow tr = new TableHeaderRow();
            //tr.Attributes.Add("class", "text-primary text-center h4");
            TableHeaderCell cell1 = new TableHeaderCell();
            cell1.Attributes.Add("scope", "col");
            TableHeaderCell cell2 = new TableHeaderCell();
            cell2.Attributes.Add("scope", "col");
            TableHeaderCell cell3 = new TableHeaderCell();
            cell3.Attributes.Add("scope", "col");
            TableHeaderCell cell4 = new TableHeaderCell();
            cell4.Attributes.Add("scope", "col");
            TableHeaderCell cell12 = new TableHeaderCell();
            cell12.Attributes.Add("scope", "col");

            cell1.Text = "#ID";
            cell2.Text = "Tipo";
            cell3.Text = "Descripcion";
            cell4.Text = "Precio";

            cell12.Text = "Acciones";
            tr.Cells.Add(cell1);
            tr.Cells.Add(cell2);
            tr.Cells.Add(cell3);
            tr.Cells.Add(cell4);

            tr.Cells.Add(cell12);
            Tabla.Rows.Add(tr);
            int i = 1;
            try
            {
                foreach (MapPase p in sc.List())
                {
                    TableRow Atr = new TableRow();
                    TableCell Acell1 = new TableCell();
                    TableCell Acell2 = new TableCell();
                    TableCell Acell3 = new TableCell();
                    TableCell Acell4 = new TableCell();

                    Acell1.Text = i.ToString();
                    Acell1.Attributes.Add("class", "h6");
                    Acell2.Text = p.Tipo;
                    Acell3.Text = p.Descripcion;
                    Acell4.Text = "$" + p.Precio.ToString();

                    /* Acciones */
                    TableCell Acell12 = new TableCell();
                    Acell12.Attributes.Add("class", "btn-group-vertical");

                    Button btn = new Button();
                    btn.Text = "Editar";
                    btn.ID = p.IdPase.ToString() + "_editar";
                    btn.Attributes.Add("class", "btn btn-outline-success");
                    btn.Click += new EventHandler(Editar);
                    Acell12.Controls.Add(btn);

                    Button btn2 = new Button();
                    btn2.Text = "Eliminar";
                    btn2.ID = p.IdPase.ToString() + "_eliminar";
                    btn2.Attributes.Add("class", "btn btn-outline-danger");
                    btn2.Click += new EventHandler(Eliminar);
                    Acell12.Controls.Add(btn2);

                    Atr.Cells.Add(Acell1);
                    Atr.Cells.Add(Acell2);
                    Atr.Cells.Add(Acell3);
                    Atr.Cells.Add(Acell4);
                    Atr.Cells.Add(Acell12);
                    Tabla.Rows.Add(Atr);

                    i++;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);

            }
        }

        private void Eliminar(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            ITransaction transaction = sesion.BeginTransaction();
            MapPase entidad = sesion.Get<MapPase>(int.Parse((sender as Button).ID.Split('_')[0]));
            try
            {
                sesion.Delete(entidad);
                sesion.Flush();
                transaction.Commit();
                this.Page_Load(sender, e);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                transaction.Rollback();
            }
            sesion.Close();
        }

        private void Editar(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            MapPase entidad = sesion.Get<MapPase>(int.Parse((sender as Button).ID.Split('_')[0]));
            this.Session["id_pase"] = entidad.IdPase;
            Response.Redirect("Registrar.aspx");
            sesion.Close();
        }
    }
}