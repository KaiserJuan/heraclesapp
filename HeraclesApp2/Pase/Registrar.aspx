﻿<%@ Page Title="Registrar Pase" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrar.aspx.cs" Inherits="HeraclesApp2.Pase.Registrar" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <h2><%: Title %>.</h2>
    <br />

    <div class="card text-white bg-secondary mb-3">
        <div class="card-header">Registre un Pase</div>
        <div class="card-body">
            <h5 class="card-title">Datos del Pase</h5>
            <p class="card-text">Todos los datos del pase son obligatorios ya que estos son usados en diferentes partes del programa.</p>
            <div>

              <div class="form-group form-row">
                    <div class="col">
                      <label for="input1Tipo">* Tipo</label>
                      <input type="text" class="form-control" runat="server" id="input1Tipo" placeholder="Tipo">
                    </div>
                    <div class="col">
                      <label for="input2Precio">* Precio</label>
                      <input type="number" class="form-control" runat="server" id="input2Precio" placeholder="Precio">
                    </div>
              </div>
            <div class="form-group form-row">
                <div class="col">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Campo Requerido" ControlToValidate="input1Tipo"></asp:RequiredFieldValidator>
                </div>
                <div class="col">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo Requerido" ControlToValidate="input2Precio"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group  input-group">
                <div class="input-group-prepend">
                <span class="input-group-text">
                    * Descripcion</span>
                </div>
                <textarea class="form-control" runat="server" id="input3Descripcion"  aria-label="Descripcion"></textarea>
            </div>
 
              <div class="text-right">
                  <asp:Button runat="server" ID="action1Registrar" Text="Registrar" OnClick="Registar" CssClass="btn btn-primary btn-lg"/>
              </div>
            </div>
        </div>
    </div>
</asp:Content>