﻿using HeraclesApp2.Entidades;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeraclesApp2.Pase
{
    public partial class Registrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Registar(object sender, EventArgs e)
        {
            ISessionFactory sesiones = new NHibernate.Cfg.Configuration().Configure().BuildSessionFactory();
            ISession sesion = sesiones.OpenSession();
            ITransaction transaction = sesion.BeginTransaction();
            MapPase pase = new MapPase();
            pase.Tipo = input1Tipo.Value;
            pase.Descripcion = input3Descripcion.Value;

            try
            {
                pase.Precio = float.Parse(input2Precio.Value);
                sesion.Save(pase);
                sesion.Flush();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                transaction.Rollback();
            }

            sesion.Close();
            sesiones.Close();
        }
    }
}